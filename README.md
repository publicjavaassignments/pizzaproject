# PizzaProject

Write a java console application where the user can order a single pizza from a small menu card.
Like this:
1.  Margheritha: Tomato, cheese Price DKK 60
2. Hawaii: Tomato, cheese, ham, pineapple Price DKK 75
3. Italiano: Tomato, cheese, pepperoni Price DKK 70
.
.
10. Special Tomato anchovies, crab Price DKK 80
Etc… 10 pizzas must be on the menu!

## Requirements:
- Only one pizza can be ordered at the time. The menu is to be presented as text for the user.
- After the number of the pizza is chosen, it must also be possible to choose extra toppings like: Onions, Cheese, Mushrooms.
- Furthermore. the user must be able to choose the size of the pizza, either child, standard or family


The prices in the menu above is the price for a standard pizza and:
size child cost 75% of the standard price.
size family costs 150% of the standard price.
Finally, the total order including the price has to be calculated and printed out on the screen in a nicely formatted way.