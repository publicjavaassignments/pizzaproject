import java.util.Scanner;

public class PizzaClass {
    public static void main(String[] args) {
        // Initialize scanner
        Scanner input = new Scanner(System.in);

        // Calls welcome and menu functions
        printWelcome();
        printMenu();

        // Declares variables
        int selectPizza;
        String selectedPizza = "";
        double pizzaPrice = 0;

        // Creating a while loop for selecting the pizzas.
        boolean selectingPizza = true;
        while(selectingPizza)
        {
            if (input.hasNextInt()) {
                selectPizza = input.nextInt();
                switch (selectPizza) {
                    case 1:
                        selectedPizza = "Margherita";
                        System.out.println("You have selected Pizza Margherita");
                        pizzaPrice = 45;
                        selectingPizza = false;
                        break;
                    case 2:
                        selectedPizza = "Pepperoni";
                        System.out.println("You have selected pizza Pepperoni");
                        pizzaPrice = 50;
                        selectingPizza = false;
                        break;
                    case 3:
                        selectedPizza = "Prosciutto";
                        System.out.println("You have selected pizza Prosciutto");
                        pizzaPrice = 50;
                        selectingPizza = false;
                        break;
                    case 4:
                        selectedPizza = "Chorizo";
                        System.out.println("You have selected pizza Chorizo");
                        pizzaPrice = 50;
                        selectingPizza = false;
                        break;
                    case 5:
                        selectedPizza = "Armandos";
                        System.out.println("You have selected pizza Armandos");
                        pizzaPrice = 60;
                        selectingPizza = false;
                        break;
                    case 6:
                        selectedPizza = "Tartufo";
                        System.out.println("You have selected pizza Tartufo");
                        pizzaPrice = 75;
                        selectingPizza = false;
                        break;
                    case 7:
                        selectedPizza = "Italiana";
                        System.out.println("You have selected pizza Italiana");
                        pizzaPrice = 50;
                        selectingPizza = false;
                        break;
                    case 8:
                        selectedPizza = "La Salame";
                        System.out.println("You have selected Pizza La Salame");
                        pizzaPrice = 55;
                        selectingPizza = false;
                        break;
                    case 9:
                        selectedPizza = "Calzone";
                        System.out.println("You have selected pizza Calzone");
                        pizzaPrice = 60;
                        selectingPizza = false;
                        break;
                    case 10:
                        selectedPizza = "Hawaii";
                        System.out.println("You have selected pizza Hawaii");
                        pizzaPrice = 70;
                        selectingPizza = false;
                        break;
                    default:
                        System.out.println("The number you have entered is not a valid pizza, please try again!");
                        break;
                }
            }else
            {
                System.out.println("Invalid input, please enter the number of your desired pizza.");
                input.next();
            }
        }

        // Calculating the pizza prices depending on the size of the pizza.
        double pizzaSizeChild = pizzaPrice * 0.75;
        double pizzaSizeNormal = pizzaPrice;
        double pizzaSizeFamily = pizzaPrice * 1.5;
        double totalPizzaPrice = 0;

        // Input and input validation for selecting the size of the pizza.
        boolean selectingSize = true;
        String pizzaSize = "";
        String test = input.nextLine();
        while(selectingSize)
        {
            System.out.println("Please select a size for your pizza: (Child, Normal, Family)");
            String selectSize = input.nextLine().toLowerCase();
            switch (selectSize) {
                case "child":
                    totalPizzaPrice = pizzaSizeChild;
                    System.out.println("You have selected a child size " + selectedPizza);
                    pizzaSize = "child size ";
                    selectingSize = false;
                    break;
                case "normal":
                    totalPizzaPrice = pizzaSizeNormal;
                    System.out.println("You have selected a normal size " + selectedPizza);
                    pizzaSize = "normal size ";
                    selectingSize = false;
                    break;
                case "family":
                    totalPizzaPrice = pizzaSizeFamily;
                    System.out.println("You have selected a family size " + selectedPizza);
                    pizzaSize = "family size ";
                    selectingSize = false;
                    break;
                default:
                    System.out.println("Invalid input, please enter your desired size of pizza.");
                    break;
            }
        }

        // Prints the list of possible toppings.
        showToppings();

        // Declaring variables for the toppings loop.
        double totalToppings = 0;
        boolean selectingToppings = true;
        boolean selectedCheese = false;
        boolean selectedDressing = false;
        boolean selectedOlives = false;
        boolean selectedMushrooms = false;
        boolean selectedJalapenos = false;
        boolean selectedBasil = false;
        boolean selectedChili = false;
        String topping = "";
        int selectingTopping;

        // Toppings while loop, so the user can select multiple toppings and have it displayed to them progressively.
        while (selectingToppings) {
            selectingTopping = input.nextInt();
            if (selectingTopping == 1 && !selectedCheese)
            {
                System.out.println("You have selected extra cheese.");
                topping = topping + "Cheese, ";
                System.out.println("Selected toppings: " + topping);
                selectedCheese = true;
                totalToppings++;
            }else if(selectingTopping == 2 && !selectedDressing)
            {
                System.out.println("You have selected cream fraiche dressing");
                topping = topping + "Dressing, ";
                System.out.println("Selected toppings: " + topping);
                selectedDressing = true;
                totalToppings++;
            }else if(selectingTopping == 3 && !selectedOlives)
            {
                System.out.println("You have selected black olives");
                topping = topping + "Olives, ";
                System.out.println("Selected toppings: " + topping);
                selectedOlives = true;
                totalToppings++;
            }else if(selectingTopping == 4 && !selectedMushrooms)
            {
                System.out.println("You have selected mushrooms");
                topping = topping + "Mushrooms, ";
                System.out.println("Selected toppings: " + topping);
                selectedMushrooms = true;
                totalToppings++;
            }else if(selectingTopping== 5 && !selectedJalapenos)
            {
                System.out.println("You have selected jalapenos");
                topping = topping + "Jalapenos, ";
                System.out.println("Selected toppings: " + topping);
                selectedJalapenos = true;
                totalToppings++;
            }else if(selectingTopping == 6 && !selectedBasil)
            {
                System.out.println("You have selected basil");
                topping = topping + "Basil leaves, ";
                System.out.println("Selected toppings: " + topping);
                selectedBasil = true;
            }else if(selectingTopping == 7 && !selectedChili)
            {
                System.out.println("You have selected chili");
                topping = topping + "Chili, ";
                System.out.println("Selected toppings: " + topping);
                selectedChili = true;
            }else if(selectingTopping == 0 && totalToppings >= 1)
            {
                System.out.println("You have selected " + topping);
                selectingToppings = false;
            }else if(selectingTopping == 0 && totalToppings == 0)
            {
                System.out.println("You have not selected any toppings");
                selectingToppings = false;
            }else
            {
                System.out.println("Invalid input (you might have already added this topping or you entered the wrong number).");
            }
        }

        // Out prints the order
        System.out.println("//////////////////////// Receipt \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
        System.out.println("You have selected a " + pizzaSize + selectedPizza);
        if(totalToppings >= 1)
        {
            System.out.println("Your added toppings are: " + topping);
        }
        System.out.printf("\nYour pizza costs: %.2f DKK", + totalPizzaPrice);
        if(totalToppings >= 1)
        {
            System.out.printf("\nYour selected toppings costs: %.2f DKK", + totalToppings * 5);
            totalPizzaPrice += totalToppings * 5;
        }
        System.out.printf("\nTotal VAT: %.2f DKK", + totalPizzaPrice * 0.25);
        System.out.printf("\nYour order total is: %.2f DKK", + totalPizzaPrice);
        System.out.println("\n//////////////////////// Receipt \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
    }
    // Print methods so it looks nicer.
    private static void printWelcome() {
        // Initialization
        System.out.println("\n--------------------------------------");
        System.out.println("Welcome to Tony's pizzeria!");
        System.out.println("Please select one of the many pizzas!");
        System.out.println("--------------------------------------");
    }

    private static void printMenu() {
        // Pizza price declarations
        int margheritaPrice = 45;
        int pepperoniPrice = 50;
        int prosciuttoPrice = 50;
        int chorizoPrice = 50;
        int armandosPrice = 60;
        int tartufoPrice = 75;
        int italianaPrice = 50;
        int laSalamePrice = 55;
        int calzonePrice = 60;
        int hawaiiPrice = 70;

        // Declaring pizzas
        String Margherita = "1. Pizza Margherita: Tomato, cheese - Price: " + margheritaPrice + " DKK";
        String Pepperoni = "2. Pizza Pepperoni:  Tomato, cheese, pepperoni - Price: " + pepperoniPrice + " DKK";
        String Prosciutto = "3. Pizza Prosciutto: Tomato, cheese, ham - Price: " + prosciuttoPrice + " DKK";
        String Chorizo = "4. Pizza Chorizo:    Tomato, cheese, spicy chorizo - Price: " + chorizoPrice + " DKK";
        String Armandos = "5. Pizza Armandos:   Tomato, cheese, prosciutto, arugula, parmesan - Price: " + armandosPrice + " DKK";
        String Tartufo = "6. Pizza Tartufo:    Tomato, cheese, bresaola, arugula, parmesan, truffle - Price: " + tartufoPrice + " DKK";
        String Italiana = "7. Pizza Italiana:   Tomato, cheese, meatsauce, oninons - Price: " + italianaPrice + " DKK";
        String La_Salame = "8. Pizza La Salame:  Tomato, cheese, pepperoni, prosciutto - Price: " + laSalamePrice + " DKK";
        String Calzone = "9. Calzone:          Tomato, cheese, prosciutto, mushrooms - Price: " + calzonePrice + " DKK";
        String Hawaii = "10. Pizza Hawaii:    Tomato, cheese, prosciutto, pineapple - Price: " + hawaiiPrice + " DKK";

        // Prints out the menu to the user.
        System.out.println(Margherita);
        System.out.println(Pepperoni);
        System.out.println(Prosciutto);
        System.out.println(Chorizo);
        System.out.println(Armandos);
        System.out.println(Tartufo);
        System.out.println(Italiana);
        System.out.println(La_Salame);
        System.out.println(Calzone);
        System.out.println(Hawaii);
    }

    private static void showToppings() {
        // Toppings message to user
        System.out.println("\nHere are extra toppings you can add to your pizza.");

        // Toppings price declarations
        int toppingPrice = 5;

        // Topping strings
        String extraCheese = "1. Extra cheese - Price: " + toppingPrice + " DKK";
        String dressing = "2. Cream fraiche dressing - Price: " + toppingPrice + " DKK";
        String olives = "3. Black olives - Price: " + toppingPrice + " DKK";
        String mushrooms = "4. Mushrooms - Price: " + toppingPrice + " DKK";
        String jalapenos = "5. Jalapenos - Price: " + toppingPrice + " DKK";
        String basil = "6. Fresh basil leaves - Price: Free";
        String chiliGarlic = "7. Chili and garlic oil - Price: Free";

        // Prints out toppings
        System.out.println(extraCheese);
        System.out.println(dressing);
        System.out.println(olives);
        System.out.println(mushrooms);
        System.out.println(jalapenos);
        System.out.println(basil);
        System.out.println(chiliGarlic);

        System.out.println("Please select your desired toppings, if you're finished or do not wish toppings, write 0");

    }
}