import java.util.Scanner;

public class Main
    {
        // * * * * * * * * * * * * * START DECLARATIONS * * * * * * * * * * * * * * * * //

        // Allocate arrays for later use in methods
        final public static String[] pizzaIngredients = new String[10];
        final public static String[] pizzaNames = new String[10];
        final public static int[] pizzaPrices = new int[10];
        final public static String[] pizzaToppingsNames = new String[5];
        final public static int[] pizzaToppingsPrices = new int[5];

        // Define standard size price differences
        final static double sizeChild = 0.75;
        final static double sizeStandard = 1;
        final static double sizeFamily = 1.5;

        // Strings to append in front, depending on the picked size.
        final static String sizeChildString = "Child sized";
        final static String sizeStandardString = "Standard sized";
        final static String sizeFamilyString = "Family sized";

        private static void defineMenu() // This is a helper method to populate the arrays defined earlier
        {
            // PRICE CHANGES SHOULD HAPPEN HERE TO PROPOGATE THROUGH ENTIRE SYSTEM!

            int i = 0;
            int j = 0;
            int k = 0;
            int l = 0;
            int o = 0;
            // Define pizza specifications in arrays
            pizzaIngredients[i++] = "With tomato, cheese";
            pizzaIngredients[i++] = "With tomato, cheese, ham and pineapple";
            pizzaIngredients[i++] = "With tomato, cheese and pepperoni";
            pizzaIngredients[i++] = "With tomato, cheese, turkeyham, onions and oregano";
            pizzaIngredients[i++] = "With tomato, cheese, maize, bacon and oregano";
            pizzaIngredients[i++] = "With tomato, cheese, pepperoni, salami, chili and oregano";
            pizzaIngredients[i++] = "With tomato, cheese, kebab, salad, chili, allium dressing and oregano";
            pizzaIngredients[i++] = "With tomato, cheese, bacon, shrimp, mushrooms and oregano";
            pizzaIngredients[i++] = "With tomato, cheese, clams, shrimp, tuna, oregano";
            pizzaIngredients[i++] = "With tomato, cheese, mushrooms, peppers, olives, oregano and cucumber";

            pizzaNames[j++] = "Margheritha";
            pizzaNames[j++] = "Hawaii";
            pizzaNames[j++] = "Italiano";
            pizzaNames[j++] = "Capricciosa";
            pizzaNames[j++] = "Isabella";
            pizzaNames[j++] = "Hot Shot";
            pizzaNames[j++] = "Al torino";
            pizzaNames[j++] = "Viking";
            pizzaNames[j++] = "Seafood";
            pizzaNames[j++] = "Vegetariana";

            pizzaPrices[k++] = 60;
            pizzaPrices[k++] = 75;
            pizzaPrices[k++] = 70;
            pizzaPrices[k++] = 50;
            pizzaPrices[k++] = 60;
            pizzaPrices[k++] = 55;
            pizzaPrices[k++] = 60;
            pizzaPrices[k++] = 55;
            pizzaPrices[k++] = 65;
            pizzaPrices[k++] = 60;

            pizzaToppingsNames[l++] = "Onions";
            pizzaToppingsNames[l++] = "Cheese";
            pizzaToppingsNames[l++] = "Mushrooms";
            pizzaToppingsNames[l++] = "Ham";
            pizzaToppingsNames[l++] = "Salad";

            pizzaToppingsPrices[o++] = 5;
            pizzaToppingsPrices[o++] = 5;
            pizzaToppingsPrices[o++] = 5;
            pizzaToppingsPrices[o++] = 5;
            pizzaToppingsPrices[o++] = 5;

        }
        // * * * * * * * * * * * * * START DECLARATIONS * * * * * * * * * * * * * * * * //

        public static void main(String[] args)
            {
                // Define pizza selection variable to fetch array index
                // Define writeable variables
                double toppingTotalPrice = 0;
                double pizzaTotalPrice = -1;
                double calculatedVAT;

                // Define bools for additional toppings
                boolean pickedOnions = false;
                boolean pickedCheese = false;
                boolean pickedMushrooms = false;
                boolean pickedHam = false;
                boolean pickedSalad = false;

                int pickedSize;
                int pickedPizza;

                Scanner keyboard = new Scanner(System.in);

                printBanner();

                defineMenu(); // Fill the arrays
                printMenuCard(); // Print the arrays

                // Determine pizza!
                do
                    {
                        while (!keyboard.hasNextInt())
                            {
                                String input = keyboard.next();
                                System.out.printf("\"%s\" is not on the menu!\n", input);
                            }
                        pickedPizza = keyboard.nextInt();
                        pickedPizza = pickedPizza - 1;
                        if (pickedPizza < 0 || pickedPizza >= pizzaNames.length)
                            {
                                System.out.println(pickedPizza + 1 + " is not a number on the menu!");
                            }
                    } while (pickedPizza < 0 || pickedPizza >= pizzaNames.length);

                clearScreen();
                System.out.println("Your pizza of choice: " + pizzaNames[pickedPizza]);
                pizzaTotalPrice = pizzaPrices[pickedPizza];

                // Determine size!

                System.out.println("Now pick your size");

                // Long print to compensate for price differences between sizes!
                System.out.println("(1) Standard sized pizza" + " - " + (pizzaPrices[pickedPizza] * sizeStandard) +
                        " DKKR" + "\n(2) Child sized pizza" + " - " + (pizzaPrices[pickedPizza] * sizeChild) +
                        " DKKR" + "\n(3) Family sized pizza" + " - " + (pizzaPrices[pickedPizza] * sizeFamily) + " DKKR");

                do
                    {
                        while (!keyboard.hasNextInt())
                            {
                                String input = keyboard.next();
                                System.out.printf("\"%s\" is not applicable!\n", input);
                            }
                        pickedSize = keyboard.nextInt();
                        if (pickedSize < 1 || pickedSize > 3)
                            {
                                System.out.println(pickedSize + " is not a valid size!");
                            }
                    } while (pickedSize < 1 || pickedSize > 3);

                clearScreen();
                if (pickedSize == 1)
                    {
                        System.out.println("Picked a standard sized pizza!");
                        pizzaTotalPrice = pizzaTotalPrice * sizeStandard;
                    } else if (pickedSize == 2)
                    {
                        System.out.println("Picked a child sized pizza!");
                        pizzaTotalPrice = pizzaTotalPrice * sizeChild;
                    } else if (pickedSize == 3)
                    {
                        System.out.println("Picked a family sized pizza!");
                        pizzaTotalPrice = pizzaTotalPrice * sizeFamily;
                    } else
                    {
                        System.out.println("Something went terribly wrong.");
                    }

                // Determine toppings!
                System.out.println("Now pick additional toppings!\n" +
                        "If you don't want any or you're done, simply write 0.\n");

                printToppingOptions();

                int pickedToppingBuffer; // Temporary place to store the input int
                do
                    {
                        while (!keyboard.hasNextInt())
                            {
                                String input = keyboard.next();
                                System.out.printf("\"%s\" is a not a topping, nor an integer!\n", input);
                            }
                        pickedToppingBuffer = keyboard.nextInt();
                        if (pickedToppingBuffer < 0 || pickedToppingBuffer > 5)
                            {
                                System.out.println(pickedToppingBuffer + " is not a valid topping!");
                            }

                        // Really inelegant set of if statements to toggle bools for topping choices
                        else if (pickedToppingBuffer == 1)
                            {
                                if (pickedOnions == true)
                                    {
                                        System.out.println("You already picked onions!");
                                    } else
                                    {
                                        pickedOnions = true;
                                        System.out.println("Added onions!");
                                        toppingTotalPrice = toppingTotalPrice + pizzaToppingsPrices[0];
                                    }

                            } else if (pickedToppingBuffer == 2)
                            {
                                if (pickedCheese == true)
                                    {
                                        System.out.println("You already picked cheese!");
                                    } else
                                    {
                                        pickedCheese = true;
                                        System.out.println("Added cheese!");
                                        toppingTotalPrice = toppingTotalPrice + pizzaToppingsPrices[1];
                                    }

                            } else if (pickedToppingBuffer == 3)
                            {
                                if (pickedMushrooms == true)
                                    {
                                        System.out.println("You already picked mushrooms!");
                                    } else
                                    {
                                        pickedMushrooms = true;
                                        System.out.println("Added mushrooms!");
                                        toppingTotalPrice = toppingTotalPrice + pizzaToppingsPrices[2];
                                    }

                            } else if (pickedToppingBuffer == 4)
                            {
                                if (pickedHam == true)
                                    {
                                        System.out.println("You already picked ham!");
                                    } else
                                    {
                                        pickedHam = true;
                                        System.out.println("Added ham!");
                                        toppingTotalPrice = toppingTotalPrice + pizzaToppingsPrices[3];
                                    }

                            } else if (pickedToppingBuffer == 5)
                            {
                                if (pickedSalad == true)
                                    {
                                        System.out.println("You already picked salad!");
                                    } else
                                    {
                                        pickedSalad = true;
                                        System.out.println("Added salad!");
                                        toppingTotalPrice = toppingTotalPrice + pizzaToppingsPrices[4];
                                    }
                            }
                    } while (pickedToppingBuffer != 0);

                clearScreen();

                // Print the final bill! Shorthand notation would make it all a lot more dense and clean. But we're
                // going to use full statements for the sake of understandability.
                System.out.println("* - - * - - * Payment Bill * - - * - - *");
                if (pickedSize == 1)
                    {
                        System.out.print("Standard sized ");
                    } else if (pickedSize == 2)
                    {
                        System.out.print("Child sized ");
                    } else if (pickedSize == 3)
                    {
                        System.out.print("Family sized ");
                    }
                System.out.print(pizzaNames[pickedPizza] + " - " + pizzaTotalPrice + " DKKR");

                // Check topping conditions
                if (pickedCheese == false && pickedHam == false && pickedMushrooms == false
                        && pickedOnions == false && pickedSalad == false)
                    {
                        System.out.println("No additional toppings picked!");
                    } else
                    {
                        System.out.println("\nToppings total: " + toppingTotalPrice + " DKKR");
                        System.out.println("\nAdditional topping(s):");
                        // I hardcoded the array indexes here since I wasn't sure how to store the index given booleans as
                        // input, and I didn't want to make a secondary integer assignment for the sake of re-referencing it
                        // I feel that just makes it even more confusing than it already is.
                        if (pickedOnions == true)
                            {
                                System.out.println(pizzaToppingsNames[0] + " - " + pizzaToppingsPrices[0] + " DKKR");
                            } else
                            {
                                // Do nothing
                            }
                        if (pickedCheese == true)
                            {
                                System.out.println(pizzaToppingsNames[1] + " - " + pizzaToppingsPrices[1] + " DKKR");
                            } else
                            {
                                // Do nothing
                            }
                        if (pickedMushrooms == true)
                            {
                                System.out.println(pizzaToppingsNames[2] + " - " + pizzaToppingsPrices[2] + " DKKR");
                            } else
                            {
                                // Do nothing
                            }
                        if (pickedHam == true)
                            {
                                System.out.println(pizzaToppingsNames[3] + " - " + pizzaToppingsPrices[3] + " DKKR");
                            } else
                            {
                                // Do nothing
                            }
                        if (pickedSalad == true)
                            {
                                System.out.println(pizzaToppingsNames[4] + " - " + pizzaToppingsPrices[4] + " DKKR");
                            } else
                            {
                                // Do nothing
                            }
                        // Handle vat and output totals
                        pizzaTotalPrice = toppingTotalPrice + pizzaTotalPrice;
                        calculatedVAT = pizzaTotalPrice * 0.25;
                        // Losing a decimal here
                        System.out.printf("\nVAT: %.2f DKKR\n", +calculatedVAT);
                        pizzaTotalPrice = (pizzaTotalPrice + calculatedVAT);
                        System.out.printf("Total: %.2f DKKR", +pizzaTotalPrice);
                    }
            }

        // * * * * * * * * * * * * * START LOGIC * * * * * * * * * * * * * * * * * * * //

        public static void printMenuCard() // I probably don't save a lot of code from this, but it felt more "right"
        {
            System.out.println("Please select a pizza by inputting the number adjacent to the name you desire:\n");

            for (int i = 0; i < pizzaNames.length; i++)
                {
                    int fixOffset = i + 1; // Fix 0-index offset so it's 1, gotta compensate for this later
                    String pizzaArrayIndexNames = pizzaNames[i];
                    System.out.print("(" + fixOffset + ") " + pizzaArrayIndexNames); // Print menu item

                    int pizzaArrayIndexPrices = pizzaPrices[i];
                    System.out.print(" - " + pizzaArrayIndexPrices + " DKKR\n"); // print price next to name

                    String pizzaArrayIndexIngredients = pizzaIngredients[i];
                    System.out.println(pizzaArrayIndexIngredients + "\n"); // print ingredients under name & price
                }
        }

        public static void printBanner()
            {
                System.out.println("  ______                              _       _       _____ _                   _       \n" +
                        " |  ____|                            | |     ( )     |  __ (_)                 (_)      \n" +
                        " | |__ ___ _ __ _ __   __ _ _ __   __| | ___ |/ ___  | |__) | __________ _ _ __ _  __ _ \n" +
                        " |  __/ _ \\ '__| '_ \\ / _` | '_ \\ / _` |/ _ \\  / __| |  ___/ |_  /_  / _` | '__| |/ _` |\n" +
                        " | | |  __/ |  | | | | (_| | | | | (_| | (_) | \\__ \\ | |   | |/ / / / (_| | |  | | (_| |\n" +
                        " |_|  \\___|_|  |_| |_|\\__,_|_| |_|\\__,_|\\___/  |___/ |_|   |_/___/___\\__,_|_|  |_|\\__,_|");
            }

        public static void printToppingOptions()
            {
                for (int i = 0; i < pizzaToppingsNames.length; i++)
                    {
                        int fixOffset = i + 1; // Gotta remember to compensate for this later!
                        String pizzaArrayIndexNames = pizzaToppingsNames[i];
                        System.out.print("(" + fixOffset + ") " + pizzaArrayIndexNames);

                        int pizzaArrayIndexPrices = pizzaToppingsPrices[i];
                        System.out.print(" - " + pizzaArrayIndexPrices + " DKKR\n");
                    }
            }

        // * * * * * * * * * * * * * END FUNCTIONALITY * * * * * * * * * * * * * * * * //

        // Universal helper methods
        public static void clearScreen() // Helper method to clear the console between selections
            {
                String os = System.getProperty("os.name").toLowerCase();
                if (os.contains("nix"))
                    {
                        System.out.print("\033\143"); // ANSI escape code, clear buffer followed by "home" to return the
                        // cursor back to the start. Only works on terminal emulators with ANSI spec support.
                    } else
                    {
                        // Do nothing, I haven't got a clue for console clearing on NT systems.
                    }
            }
    }
